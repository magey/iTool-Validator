# iTool-Validator

#### 介绍
 - 用于netcore api项目数据验证
 - 使用简单，只需要专心于业务逻辑。对源代码完全没有侵入

#### 安装教程
 - 下载示例项目 拆包即用

#### 使用有多简单？只需要2步
 - 新建验证类 TestControllerValidatorAttribute
 - 在对应的Controller上标注特性即可

#### 验证类示例
``` C#
public class TestControllerValidatorAttribute : ControllerValidatorAttribute
{
    public void Post(TestModel model)
    {
        if (model.id > 100)
            throw new Exception("我好像并不了解你");

        model.userName = "不管你是什么我都修改你，怎么着吧";
    }
}
```

#### 控制器使用示例
```
[TestControllerValidator]
public class ValuesController : ControllerBase
{
    [HttpPost]
    public ActionResult<TestModel> Post(TestModel model)
    {
        return model;
    }
}
```

#### 请求
` POST http://localhost:20412/api/values `
` DATA = {id:12} `

```javascript
// 返回信息
{
    "id": 12,
    "userName": "不管你是什么我都修改你，怎么着吧"
}

```

---
` POST http://localhost:20412/api/values `
` DATA = { id: 101, userName: "你好" } `


``` javascript
// 返回信息
{
    "code": 10003,
    "info": "我好像并不了解你",
    "data": ""
}

```

#### 自定义返回数据格式
 - 基类提供了 `ValidatorError(string error)`的默认实现

``` C#
// 重写返回方法
public override object ValidatorError(string error)
{
    return new
            {
                code = 1,
                info = error,
                data11 = "我想干嘛干嘛"
            };
}


// 则返回的数据格式为
{
    "code": 1,
    "info": "我好像并不了解你",
    "data11": "我想干嘛干嘛"
}

```

#### 关于效率的3种情况说明
 1. 第一次启动时，配置信息初始化。类目越多时间越久。 简单测试得出结果： （20 + （n*0.2））ms = ！！！ 只有第一次启动会有
 2. 出现数据不合法：响应速度高于在Controller内做验证的速度 
 3. 非第一次启动的正常反馈： ≈400微秒 = 0.4ms 可以忽略不记



 